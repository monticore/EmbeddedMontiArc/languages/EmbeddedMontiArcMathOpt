<!-- (c) https://github.com/MontiCore/monticore -->
![pipeline](https://git.rwth-aachen.de/monticore/EmbeddedMontiArc/languages/EmbeddedMontiArcMathOpt/badges/master/build.svg)
![coverage](https://git.rwth-aachen.de/monticore/EmbeddedMontiArc/languages/EmbeddedMontiArcMathOpt/badges/master/coverage.svg)

# EmbeddedMontiArcMathOpt

## :warning: WARNING :warning:
This language is deprecated. MontiMathOpt is directly integrated into EmbeddedMontiArcMath as of version 0.1.5-SNAPSHOT.

## Summary

EmbeddedMontiArcMathOpt (EMAMOpt) is a component and connector based modeling language for optimization problems. It combines the features of EmbeddedMontiArcMath (EMAM) and MontiMathOpt (MathOpt).

## Language Hierachy

EmbeddedMontiArcMathOpt extends EMAM and embedds MontiMathOpt. 
Thus it contains all features of EmbeddedMontiArc (EMA) and EMAM. Additionally it adds the ability to define components which describe optimization problems.

![Language Hierachy](https://git.rwth-aachen.de/monticore/EmbeddedMontiArc/languages/EmbeddedMontiArcMathOpt/raw/master/doc/emamopt.png)

## Development Hints

The main challange was to combine the symbol tables of EMAM and MathOpt. 
These were delegated in [EmbeddedMontiArcMathOptSymbolTableCreator.java](https://git.rwth-aachen.de/monticore/EmbeddedMontiArc/languages/EmbeddedMontiArcMathOpt/blob/master/src/main/java/de/monticore/lang/embeddedmontiarcmathopt/_symboltable/EmbeddedMontiArcMathOptSymbolTableCreator.java)

## See Also

[MathOpt](https://git.rwth-aachen.de/monticore/EmbeddedMontiArc/languages/MontiMathOpt)

[EMAMOpt2Cpp](https://git.rwth-aachen.de/monticore/EmbeddedMontiArc/generators/EMAMOpt2Cpp)

[Master-Thesis](https://git.rwth-aachen.de/monticore/EmbeddedMontiArc/languages/MontiMathOpt/blob/master/doc/master_thesis_richter.pdf)
