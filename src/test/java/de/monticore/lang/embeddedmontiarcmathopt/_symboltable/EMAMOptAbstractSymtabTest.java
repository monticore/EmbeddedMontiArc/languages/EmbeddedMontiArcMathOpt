/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.embeddedmontiarcmathopt._symboltable;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarcmath.AbstractSymtabTest;
import de.monticore.symboltable.Scope;

/**
 * Base test class for  EMAMOpt symbol tables
 * Provides method to create a symbol table
 */
public abstract class EMAMOptAbstractSymtabTest extends AbstractSymtabTest {

    protected static Scope createSymTab(String... modelPath) {
        return EMAMOptSymbolTableHelper.getInstance().createSymTab(modelPath);
    }

}
