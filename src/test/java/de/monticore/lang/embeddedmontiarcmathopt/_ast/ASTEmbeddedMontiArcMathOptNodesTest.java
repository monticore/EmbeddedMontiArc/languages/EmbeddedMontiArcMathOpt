/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.embeddedmontiarcmathopt._ast;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarcbehavior._ast.ASTBehaviorImplementation;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarcmath._ast.ASTBehaviorEmbedding;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarcmath._ast.ASTEMAMCompilationUnit;
import de.monticore.lang.embeddedmontiarcmathopt._parser.EmbeddedMontiArcMathOptParser;
import de.monticore.lang.math._ast.ASTStatement;
import de.monticore.lang.mathopt._ast.ASTOptimizationStatement;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 */
public class ASTEmbeddedMontiArcMathOptNodesTest {

    @Test
    public void testEMAMOptCompilationUnit() throws IOException {
        EmbeddedMontiArcMathOptParser parser = new EmbeddedMontiArcMathOptParser();
        ASTEMAMCompilationUnit astNode = parser.parse("src/test/resources/test/emam/optimization/MinimizePortsTest.emam").orElse(null);
        assertNotNull(astNode);
        assertEquals(ASTBehaviorImplementation.class, astNode.getEMACompilationUnit().getComponent().getBody().getElement(1).getClass());
        ASTBehaviorImplementation behaviour = (ASTBehaviorImplementation) astNode.getEMACompilationUnit().getComponent().getBody().getElement(1);
        ASTStatement astOptStatement = ((ASTBehaviorEmbedding) behaviour.getBehavior()).getStatement(0);
        assertEquals(ASTOptimizationStatement.class, astOptStatement.getClass());
    }
}
