/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.embeddedmontiarcmathopt._parser;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarcmath.ParserTest;

public class EmbeddedMontiArcMathOptParserTest extends ParserTest {

    @Override
    public void setUp() {
        super.setUp();
        setParser(new EmbeddedMontiArcMathOptParser());
    }
}
