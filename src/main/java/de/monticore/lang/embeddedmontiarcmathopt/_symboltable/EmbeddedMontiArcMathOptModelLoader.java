/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.embeddedmontiarcmathopt._symboltable;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarcmath._ast.ASTEMAMCompilationUnit;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarcmath._symboltable.EmbeddedMontiArcMathModelLoader;
import de.monticore.modelloader.ModelingLanguageModelLoader;
import de.monticore.symboltable.ArtifactScope;
import de.monticore.symboltable.MutableScope;
import de.monticore.symboltable.ResolvingConfiguration;
import de.monticore.symboltable.Scope;
import de.se_rwth.commons.logging.Log;

public class EmbeddedMontiArcMathOptModelLoader extends ModelingLanguageModelLoader<ASTEMAMCompilationUnit> {

    public EmbeddedMontiArcMathOptModelLoader(EmbeddedMontiArcMathOptLanguage language) {
        super(language);
    }

    @Override
    protected void createSymbolTableFromAST(ASTEMAMCompilationUnit ast, String modelName, MutableScope enclosingScope, ResolvingConfiguration resolvingConfiguration) {
        final EmbeddedMontiArcMathOptSymbolTableCreator stc = getModelingLanguage().getSymbolTableCreator(resolvingConfiguration, enclosingScope).orElse(null);

        if (stc != null) {
            Log.debug("Start creation of symbol table for model \"" + modelName + "\".",
                    EmbeddedMontiArcMathModelLoader.class.getSimpleName());
            final Scope scope = stc.createFromAST(ast);

            if (!(scope instanceof ArtifactScope)) {
                Log.warn("0xA7001_184 Top scope of model " + modelName + " is expected to be an artifact scope, but"
                        + " is scope \"" + scope.getName() + "\"");
            }

            Log.debug("Created symbol table for model \"" + modelName + "\".", EmbeddedMontiArcMathOptModelLoader.class.getSimpleName());
        } else {
            Log.warn("0xA7002_184 No symbol created, because '" + getModelingLanguage().getName()
                    + "' does not define a symbol table creator.");
        }
    }

    @Override
    public EmbeddedMontiArcMathOptLanguage getModelingLanguage() {
        return (EmbeddedMontiArcMathOptLanguage) super.getModelingLanguage();
    }
}
