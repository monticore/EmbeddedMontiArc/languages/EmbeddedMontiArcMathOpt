/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.embeddedmontiarcmathopt._symboltable;

import de.monticore.EmbeddingModelingLanguage;
import de.monticore.antlr4.MCConcreteParser;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.EmbeddedMontiArcLanguage;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarcmath._symboltable.EmbeddedMontiArcMathLanguage;
import de.monticore.lang.embeddedmontiarcmathopt._parser.EmbeddedMontiArcMathOptParser;
import de.monticore.lang.mathopt._symboltable.MathOptLanguage;
import de.monticore.symboltable.MutableScope;
import de.monticore.symboltable.ResolvingConfiguration;
import de.monticore.symboltable.Symbol;
import de.monticore.symboltable.resolving.ResolvingFilter;

import java.util.Collection;
import java.util.Optional;

public class EmbeddedMontiArcMathOptLanguage extends EmbeddingModelingLanguage {

    private EmbeddedMontiArcMathLanguage parentLanguage;

    public EmbeddedMontiArcMathOptLanguage() {
        super("Embedded MontiArc Math Opt Language", "emam", new EmbeddedMontiArcLanguage(), new MathOptLanguage());
        parentLanguage = new EmbeddedMontiArcMathLanguage();
    }

    @Override
    public Collection<ResolvingFilter<? extends Symbol>> getResolvingFilters() {
        return parentLanguage.getResolvingFilters();
    }

    @Override
    protected EmbeddedMontiArcMathOptModelLoader provideModelLoader() {
        return new EmbeddedMontiArcMathOptModelLoader(this);
    }

    @Override
    public MCConcreteParser getParser() {
        return new EmbeddedMontiArcMathOptParser();
    }

    @Override
    public Optional<EmbeddedMontiArcMathOptSymbolTableCreator> getSymbolTableCreator(ResolvingConfiguration resolvingConfiguration, MutableScope enclosingScope) {
        return Optional.of(new EmbeddedMontiArcMathOptSymbolTableCreator(resolvingConfiguration, enclosingScope));
    }
}
