/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.embeddedmontiarcmathopt._symboltable;

import de.monticore.ModelingLanguageFamily;
import de.monticore.io.paths.ModelPath;
import de.monticore.lang.embeddedmontiarc.LogConfig;
import de.monticore.lang.mathopt._symboltable.MathOptLanguage;
import de.monticore.lang.monticar.Utils;
import de.monticore.lang.monticar.enumlang._symboltable.EnumLangLanguage;
import de.monticore.lang.monticar.stream._symboltable.StreamLanguage;
import de.monticore.lang.monticar.streamunits._symboltable.StreamUnitsLanguage;
import de.monticore.lang.monticar.struct._symboltable.StructLanguage;
import de.monticore.symboltable.GlobalScope;
import de.monticore.symboltable.Scope;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Helper class to create symbol tables for EMAMOpt models.
 * Implements singleton pattern
 *
 */
public class EMAMOptSymbolTableHelper {
    protected static EMAMOptSymbolTableHelper ourInstance;

    protected EMAMOptSymbolTableHelper() {
    }

    public static EMAMOptSymbolTableHelper getInstance() {
        if (ourInstance == null)
            ourInstance = new EMAMOptSymbolTableHelper();
        return ourInstance;
    }

    public Scope createSymTab(String... modelPath) {
        ModelingLanguageFamily fam = getModelingLanguageFamily();
        ModelPath mp = getModelPath(modelPath);
        GlobalScope scope = new GlobalScope(mp, fam);
        Utils.addBuiltInTypes(scope);
        LogConfig.init();
        return scope;
    }

    public ModelingLanguageFamily getModelingLanguageFamily() {
        ModelingLanguageFamily fam = new ModelingLanguageFamily();
        fam.addModelingLanguage(new EmbeddedMontiArcMathOptLanguage());
        fam.addModelingLanguage(new MathOptLanguage());
        fam.addModelingLanguage(new StreamLanguage());
        fam.addModelingLanguage(new StructLanguage());
        fam.addModelingLanguage(new StreamUnitsLanguage());
        fam.addModelingLanguage(new EnumLangLanguage());
        return fam;
    }

    public ModelPath getModelPath(String... modelPath) {
        ModelPath mp = new ModelPath(new Path[0]);
        String[] var2 = modelPath;
        int var3 = modelPath.length;
        for (int var4 = 0; var4 < var3; ++var4) {
            String m = var2[var4];
            mp.addEntry(Paths.get(m));
        }
        return mp;
    }
}
