/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.embeddedmontiarcmathopt._symboltable;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarcmath._symboltable.EmbeddedMontiArcMathSymbolTableCreator;
import de.monticore.lang.embeddedmontiarcmathopt._visitor.EmbeddedMontiArcMathOptDelegatorVisitor;
import de.monticore.lang.embeddedmontiarcmathopt._visitor.EmbeddedMontiArcMathOptVisitor;
import de.monticore.lang.mathopt._symboltable.MathOptSymbolTableCreator;
import de.monticore.symboltable.MutableScope;
import de.monticore.symboltable.ResolvingConfiguration;

import java.util.Deque;

/**
 * Creates a hand written symbol table for EmbeddedMontiArcMathOpt
 *
 */
public class EmbeddedMontiArcMathOptSymbolTableCreator extends EmbeddedMontiArcMathSymbolTableCreator implements EmbeddedMontiArcMathOptVisitor {

    // fields
    private EmbeddedMontiArcMathOptDelegatorVisitor visitorOpt;
    private MathOptSymbolTableCreator mathOptSTC;

    // constructors from inherited
    public EmbeddedMontiArcMathOptSymbolTableCreator(ResolvingConfiguration resolvingConfig, MutableScope enclosingScope) {
        super(resolvingConfig, enclosingScope);
    }

    public EmbeddedMontiArcMathOptSymbolTableCreator(ResolvingConfiguration resolvingConfig, Deque<MutableScope> scopeStack) {
        super(resolvingConfig, scopeStack);
    }

    @Override
    protected void initSuperSTC(ResolvingConfiguration resolvingConfig) {
        super.initSuperSTC(resolvingConfig);
        // init visitorOpt
        visitorOpt = new EmbeddedMontiArcMathOptDelegatorVisitor();
        // init math opt symbol table creator
        mathOptSTC = new MathOptSymbolTableCreator(resolvingConfig, scopeStack);
        // init visitor delegator for test.emam.optimization languages
        visitorOpt.setExpressionsBasisVisitor(mathOptSTC);
        visitorOpt.setCommonExpressionsVisitor(mathOptSTC);
        visitorOpt.setAssignmentExpressionsVisitor(mathOptSTC);
        visitorOpt.setTypes2Visitor(mathOptSTC);
        visitorOpt.setMatrixExpressionsVisitor(mathOptSTC);
        visitorOpt.setMatrixVisitor(mathOptSTC);
        visitorOpt.setMathVisitor(mathOptSTC);
        visitorOpt.setMathOptVisitor(mathOptSTC);
        // set existing stc's
        visitorOpt.setCommon2Visitor(getEmamSTC());
        visitorOpt.setEmbeddedMontiArcMathVisitor(getEmamSTC());
        visitorOpt.setEmbeddedMontiArcVisitor(getEmaSTC());
        visitorOpt.setEmbeddedMontiArcBehaviorVisitor(getEmaBehaviorSTC());
    }

    @Override
    public EmbeddedMontiArcMathOptVisitor getRealThis() {
        return this;
    }

    @Override
    public void setRealThis(EmbeddedMontiArcMathOptVisitor realThis) {
        if (getRealThis() != realThis) {
            setRealThis(realThis);
            visitorOpt.setRealThis(realThis);
        }
    }

    protected MathOptSymbolTableCreator getMathOptSTC() {
        return mathOptSTC;
    }

}

